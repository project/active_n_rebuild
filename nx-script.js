(function ($) {

  Drupal.behaviors.move_summary = {
    attach: function (context, settings) {
      $('fieldset .fieldset-legend > .summary', context).once('move_summary', function () {
        var summary = $(this),
            fs_legend = summary.parent(),
            cv_3 = $('.cv-3', fs_legend);
        cv_3.append(summary);
      });
    }
  };

 /* --- [EN] Example of cookie usage -------------------------------------------------------
    $.cookie('nx_param1', 'value1', {expires: 1000, path: '/'}); // for setting cookie value
    var nx_param1 = $.cookie('nx_param1');                       // for getting cookie value
    ---------------------------------------------------------------------------------------- */

})(jQuery);