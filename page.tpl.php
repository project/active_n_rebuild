
  <table class="page struct">
    <tr>
      <td class="L_Field struct">&nbsp;</td>
      <td class="M_Field struct">


        <table class="head_part page_part struct">
          <tr>
            <td class="column_1 struct">
              <!-- column 1 -->
                <div class="column_1-cv_1">
                <div class="column_1-cv_2">
                  <?php if ($logo) { ?>
                    <a id="site_logo" href="<?php print $front_page; ?>" title="<?php print $logo_title; ?>">
                      <img src="<?php print $logo; ?>" alt="<?php print $logo_title; ?>" />
                    </a><div class="clearfix"></div>
                  <?php } ?>
                </div>
                </div>
              <!-- end column 1 -->
              <div class="m_size"></div>
            </td>

            <td class="column_2 struct">
              <!-- column 2 -->
                <div class="column_2-cv_1">
                <div class="column_2-cv_2">
                  <?php print render($page['header']); ?>
                </div>
                </div>
              <!-- end column 2 -->
              <div class="m_size"></div>
            </td>

            <td class="column_3 struct<?php print !$page['head_column_3_region'] ? ' column_3-invisible' : ''; ?>">
              <!-- column 3 -->
                <div class="column_3-cv_1">
                <div class="column_3-cv_2">
                  <?php print render($page['head_column_3_region']); ?>
                </div>
                </div>
              <!-- end column 3 -->
              <div class="m_size"></div>
            </td>
          </tr>
        </table>


        <table class="body_part page_part struct">
          <tr>
            <td class="column_1 struct">
              <!-- column 1 -->
                <div class="column_1-cv_1">
                <div class="column_1-cv_2">
                  <?php if ($page['sidebar_first']) { ?>
                    <?php print render($page['sidebar_first']); ?>
                  <?php } ?>
                </div>
                </div>
              <!-- end column 1 -->
              <div class="m_size"></div>
            </td>

            <td class="column_2 struct">
              <!-- column 2 -->
                <div class="column_2-cv_1">
                <div class="column_2-cv_2">
                  <?php print $breadcrumb; ?>
                  <a id="main-content"></a>

                  <?php print render($title_prefix); ?>
                  <?php if ($title) { ?><h1 class="page_title"><?php print $title; ?></h1><?php } ?>
                  <?php print render($title_suffix); ?>

                  <?php if (render($tabs_pri) || render($tabs_sec)) { ?>
                    <div class="tabs">
                      <?php print render($tabs_pri); ?><div class="clearfix"></div>
                      <?php if ($tabs_pri || $tabs_sec) { ?><div class="tabs_bounder"></div><?php } ?>
                      <?php print render($tabs_sec); ?><div class="clearfix"></div>
                    </div>
                  <?php } ?>

                  <?php print $messages; ?>
                  <?php print render($page['help']); ?>
                  <?php if ($action_links) { ?><ul class="action-links"><?php print render($action_links); ?></ul><?php } ?>
                  <?php print render($page['content']); ?>
                </div>
                </div>
              <!-- end column 2 -->
              <div class="m_size"></div>
            </td>

            <td class="column_3 struct<?php print !$page['sidebar_second'] ? ' column_3-invisible' : ''; ?>">
              <!-- column 3 -->
                <div class="column_3-cv_1">
                <div class="column_3-cv_2">
                  <?php if ($page['sidebar_second']) { ?>
                    <?php print render($page['sidebar_second']); ?>
                  <?php } ?>
                </div>
                </div>
              <!-- end column 3 -->
              <div class="m_size"></div>
            </td>
          </tr>
        </table>


        <table class="foot_part page_part struct">
          <tr>
            <td class="column_1 struct">
              <!-- column 1 -->
                <div class="column_1-cv_1">
                <div class="column_1-cv_2">
                  <?php print render($page['foot_column_1_region']); ?>
                </div>
                </div>
              <!-- end column 1 -->
              <div class="m_size"></div>
            </td>

            <td class="column_2 struct">
              <!-- column 2 -->
                <div class="column_2-cv_1">
                <div class="column_2-cv_2">
                  <?php print render($page['footer']); ?>
                </div>
                </div>
              <!-- end column 2 -->
              <div class="m_size"></div>
            </td>

            <td class="column_3 struct<?php print !$page['foot_column_3_region'] ? ' column_3-invisible' : ''; ?>">
              <!-- column 3 -->
                <div class="column_3-cv_1">
                <div class="column_3-cv_2">
                  <?php print render($page['foot_column_3_region']); ?>
                </div>
                </div>
              <!-- end column 3 -->
              <div class="m_size"></div>
            </td>
          </tr>
        </table>


      </td>
      <td class="R_Field struct">&nbsp;</td>
    </tr>
  </table>


  <div class="techno_bottom_region">
    <?php print render($page['techno_bottom_region']); ?>
  </div>

<?php /* if ($site_slogan) print '<div id="site_slogan">'. $site_slogan .'</div>'; */ ?>
<?php /* if ($nav_pri) print $nav_pri; */ ?>
<?php /* if ($nav_sec) print $nav_sec; */ ?>
<?php /* print $feed_icons; */ ?>