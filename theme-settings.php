<?php

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */
function active_n_rebuild_form_system_theme_settings_alter(&$form, &$form_state) {
  $form['nx_theme_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Extra params'),
    '#collapsible' => FALSE,
    '#weight' => -2,
  );
  $form['nx_theme_settings']['nx_btn_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable button skin'),
    '#default_value' => theme_get_setting('nx_btn_mode'),
  );
  $form['nx_theme_settings']['nx_invisible_links'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable invisible links'),
    '#default_value' => theme_get_setting('nx_invisible_links'),
  );
}
