<fieldset <?php print $elements['attributes']; ?>>
  <?php if (!empty($elements['title'])) { ?>
    <legend><span class="fieldset-legend">
      <span class="cv-1 cv-n"><span class="cv-2 cv-n"><span class="cv-3 cv-n">
        <?php print truncate_utf8($elements['title'], 70, FALSE, TRUE); ?>
      </span></span></span>
    </span></legend>
  <?php } ?>
  <div class="fieldset-wrapper clearfix">
    <?php if (!empty($elements['description'])) { ?>
      <div class="fieldset-description"><?php print $elements['description']; ?></div>
    <?php } ?>
    <?php print $elements['children']; ?>
    <?php if (isset($elements['value'])) { ?>
      <?php print $elements['value']; ?>
    <?php } ?>
  </div>
</fieldset>