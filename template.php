<?php


  global $path_to_this_theme;
         $path_to_this_theme = drupal_get_path('theme', 'active_n_rebuild');


  $GLOBALS['NX_OVERRIDE_CSS'] = array(
    'misc/farbtastic/farbtastic.css'         => "$path_to_this_theme/override_styles/misc/farbtastic.css",
    'misc/ui/jquery.ui.accordion.css'        => "$path_to_this_theme/override_styles/misc/jquery.ui.accordion.css",
    'misc/ui/jquery.ui.autocomplete.css'     => "$path_to_this_theme/override_styles/misc/jquery.ui.autocomplete.css",
    'misc/ui/jquery.ui.button.css'           => "$path_to_this_theme/override_styles/misc/jquery.ui.button.css",
    'misc/ui/jquery.ui.core.css'             => "$path_to_this_theme/override_styles/misc/jquery.ui.core.css",
    'misc/ui/jquery.ui.datepicker.css'       => "$path_to_this_theme/override_styles/misc/jquery.ui.datepicker.css",
    'misc/ui/jquery.ui.dialog.css'           => "$path_to_this_theme/override_styles/misc/jquery.ui.dialog.css",
    'misc/ui/jquery.ui.progressbar.css'      => "$path_to_this_theme/override_styles/misc/jquery.ui.progressbar.css",
    'misc/ui/jquery.ui.resizable.css'        => "$path_to_this_theme/override_styles/misc/jquery.ui.resizable.css",
    'misc/ui/jquery.ui.selectable.css'       => "$path_to_this_theme/override_styles/misc/jquery.ui.selectable.css",
    'misc/ui/jquery.ui.slider.css'           => "$path_to_this_theme/override_styles/misc/jquery.ui.slider.css",
    'misc/ui/jquery.ui.tabs.css'             => "$path_to_this_theme/override_styles/misc/jquery.ui.tabs.css",
    'misc/ui/jquery.ui.theme.css'            => "$path_to_this_theme/override_styles/misc/jquery.ui.theme.css",
    'misc/print.css'                         => "$path_to_this_theme/override_styles/misc/print.css",
    'misc/vertical-tabs.css'                 => "$path_to_this_theme/override_styles/misc/vertical-tabs.css",
    'modules/aggregator/aggregator.css'      => "$path_to_this_theme/override_styles/modules/aggregator.css",
    'modules/block/block.css'                => "$path_to_this_theme/override_styles/modules/block.css",
    'modules/book/book.css'                  => "$path_to_this_theme/override_styles/modules/book.css",
    'modules/color/color.css'                => "$path_to_this_theme/override_styles/modules/color.css",
    'modules/comment/comment.css'            => "$path_to_this_theme/override_styles/modules/comment.css",
    'modules/contextual/contextual.css'      => "$path_to_this_theme/override_styles/modules/contextual.css",
    'modules/dashboard/dashboard.css'        => "$path_to_this_theme/override_styles/modules/dashboard.css",
    'modules/dblog/dblog.css'                => "$path_to_this_theme/override_styles/modules/dblog.css",
    'modules/field/theme/field.css'          => "$path_to_this_theme/override_styles/modules/field.css",
    'modules/field_ui/field_ui.css'          => "$path_to_this_theme/override_styles/modules/field_ui.css",
    'modules/file/file.css'                  => "$path_to_this_theme/override_styles/modules/file.css",
    'modules/filter/filter.css'              => "$path_to_this_theme/override_styles/modules/filter.css",
    'modules/forum/forum.css'                => "$path_to_this_theme/override_styles/modules/forum.css",
    'modules/help/help.css'                  => "$path_to_this_theme/override_styles/modules/help.css",
    'modules/image/image.admin.css'          => "$path_to_this_theme/override_styles/modules/image.admin.css",
    'modules/image/image.css'                => "$path_to_this_theme/override_styles/modules/image.css",
    'modules/locale/locale.css'              => "$path_to_this_theme/override_styles/modules/locale.css",
    'modules/menu/menu.css'                  => "$path_to_this_theme/override_styles/modules/menu.css",
    'modules/node/node.css'                  => "$path_to_this_theme/override_styles/modules/node.css",
    'modules/openid/openid.css'              => "$path_to_this_theme/override_styles/modules/openid.css",
    'modules/overlay/overlay-child.css'      => "$path_to_this_theme/override_styles/modules/overlay-child.css",
    'modules/overlay/overlay-parent.css'     => "$path_to_this_theme/override_styles/modules/overlay-parent.css",
    'modules/poll/poll.css'                  => "$path_to_this_theme/override_styles/modules/poll.css",
    'modules/profile/profile.css'            => "$path_to_this_theme/override_styles/modules/profile.css",
    'modules/search/search.css'              => "$path_to_this_theme/override_styles/modules/search.css",
    'modules/shortcut/shortcut.admin.css'    => "$path_to_this_theme/override_styles/modules/shortcut.admin.css",
    'modules/shortcut/shortcut.css'          => "$path_to_this_theme/override_styles/modules/shortcut.css",
    'modules/simpletest/simpletest.css'      => "$path_to_this_theme/override_styles/modules/simpletest.css",
    'modules/system/system.admin.css'        => "$path_to_this_theme/override_styles/modules/system.admin.css",
    'modules/system/system.base.css'         => "$path_to_this_theme/override_styles/modules/system.base.css",
    'modules/system/system.maintenance.css'  => "$path_to_this_theme/override_styles/modules/system.maintenance.css",
    'modules/system/system.menus.css'        => "$path_to_this_theme/override_styles/modules/system.menus.css",
    'modules/system/system.messages.css'     => "$path_to_this_theme/override_styles/modules/system.messages.css",
    'modules/system/system.theme.css'        => "$path_to_this_theme/override_styles/modules/system.theme.css",
    'modules/taxonomy/taxonomy.css'          => "$path_to_this_theme/override_styles/modules/taxonomy.css",
    'modules/toolbar/toolbar.css'            => "$path_to_this_theme/override_styles/modules/toolbar.css",
    'modules/tracker/tracker.css'            => "$path_to_this_theme/override_styles/modules/tracker.css",
    'modules/update/update.css'              => "$path_to_this_theme/override_styles/modules/update.css",
    'modules/user/user.css'                  => "$path_to_this_theme/override_styles/modules/user.css",
  /* RTL */
    'misc/print-rtl.css'                     => "$path_to_this_theme/override_styles/misc/print.css",
    'misc/vertical-tabs-rtl.css'             => "$path_to_this_theme/override_styles/misc/vertical-tabs.css",
    'modules/aggregator/aggregator-rtl.css'  => "$path_to_this_theme/override_styles/modules/aggregator.css",
    'modules/book/book-rtl.css'              => "$path_to_this_theme/override_styles/modules/book.css",
    'modules/color/color-rtl.css'            => "$path_to_this_theme/override_styles/modules/color.css",
    'modules/comment/comment-rtl.css'        => "$path_to_this_theme/override_styles/modules/comment.css",
    'modules/contextual/contextual-rtl.css'  => "$path_to_this_theme/override_styles/modules/contextual.css",
    'modules/dashboard/dashboard-rtl.css'    => "$path_to_this_theme/override_styles/modules/dashboard.css",
    'modules/dblog/dblog-rtl.css'            => "$path_to_this_theme/override_styles/modules/dblog.css",
    'modules/field_ui/field_ui-rtl.css'      => "$path_to_this_theme/override_styles/modules/field_ui.css",
    'modules/field/theme/field-rtl.css'      => "$path_to_this_theme/override_styles/modules/field.css",
    'modules/forum/forum-rtl.css'            => "$path_to_this_theme/override_styles/modules/forum.css",
    'modules/help/help-rtl.css'              => "$path_to_this_theme/override_styles/modules/help.css",
    'modules/image/image-rtl.css'            => "$path_to_this_theme/override_styles/modules/image.css",
    'modules/locale/locale-rtl.css'          => "$path_to_this_theme/override_styles/modules/locale.css",
    'modules/node/node-rtl.css'              => "$path_to_this_theme/override_styles/modules/node.css",
    'modules/openid/openid-rtl.css'          => "$path_to_this_theme/override_styles/modules/openid.css",
    'modules/overlay/overlay-child-rtl.css'  => "$path_to_this_theme/override_styles/modules/overlay-child.css",
    'modules/poll/poll-rtl.css'              => "$path_to_this_theme/override_styles/modules/poll.css",
    'modules/search/search-rtl.css'          => "$path_to_this_theme/override_styles/modules/search.css",
    'modules/shortcut/shortcut-rtl.css'      => "$path_to_this_theme/override_styles/modules/shortcut.css",
    'modules/system/system.admin-rtl.css'    => "$path_to_this_theme/override_styles/modules/system.admin.css",
    'modules/system/system.base-rtl.css'     => "$path_to_this_theme/override_styles/modules/system.base.css",
    'modules/system/system.menus-rtl.css'    => "$path_to_this_theme/override_styles/modules/system.menus.css",
    'modules/system/system.messages-rtl.css' => "$path_to_this_theme/override_styles/modules/system.messages.css",
    'modules/system/system.theme-rtl.css'    => "$path_to_this_theme/override_styles/modules/system.theme.css",
    'modules/toolbar/toolbar-rtl.css'        => "$path_to_this_theme/override_styles/modules/toolbar.css",
    'modules/update/update-rtl.css'          => "$path_to_this_theme/override_styles/modules/update.css",
    'modules/user/user-rtl.css'              => "$path_to_this_theme/override_styles/modules/user.css",
  );
  

function active_n_rebuild_theme() {
  return array(
    'nx-fieldset' => array(
      'template' => 'nx-fieldset',
      'variables' => array('elements' => NULL),
    ),
  );
}

/**
 * Implements hook_css_alter().
 */
function active_n_rebuild_css_alter(&$css) {
  foreach ($css as $idx => $item) {
    if (isset($GLOBALS['NX_OVERRIDE_CSS'][$idx])) {
      $css[$idx]['data'] = $GLOBALS['NX_OVERRIDE_CSS'][$idx];
    }
  }
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function active_n_rebuild_breadcrumb($vars) {
    $breadcrumb = $vars['breadcrumb'];
    if (!empty($breadcrumb)) {
      $output = '';
      if (theme_get_setting('nx_invisible_links')) {
        $output.= '<h2 class="element-invisible">' . t('You are here') . '</h2>';
      }
      $last_item = &$breadcrumb[count($breadcrumb)-1];
      $last_item = str_replace('<a ', '<a class="last" ', $last_item);
      $output.= '<div class="breadcrumb">' . implode(' <span class="gt">&gt;</span> ', $breadcrumb) . '</div>';
      return $output;
    }
}

/**
 * Override or insert variables into the page template.
 */
function active_n_rebuild_preprocess_page(&$vars) {

 /* Inline menu (nav_pri, nav_sec) */
    if (isset($vars['main_menu'])) {
      $nav_pri_data = array(
        'links' => $vars['main_menu'],
        'attributes' => array('class' => array('nav_pri', 'nav_pri-inline')),
      );
      if (theme_get_setting('nx_invisible_links')) {
        $nav_pri_data['heading'] = array(
          'text' => t('Primary links'),
          'level' => 'h2',
          'class' => array('element-invisible'),
        );
      }
      $vars['nav_pri'] = theme('links__system_main_menu', $nav_pri_data);
    }
    else {
      $vars['nav_pri'] = FALSE;
    }
    if (isset($vars['secondary_menu'])) {
      $nav_sec_data = array(
        'links' => $vars['secondary_menu'],
        'attributes' => array('class' => array('nav_sec', 'nav_sec-inline')),
      );
      if (theme_get_setting('nx_invisible_links')) {
        $nav_sec_data['heading'] = array(
          'text' => t('Secondary links'),
          'level' => 'h2',
          'class' => array('element-invisible'),
        );
      }
      $vars['nav_sec'] = theme('links__system_secondary_menu', $nav_sec_data);
    }
    else {
      $vars['nav_sec'] = FALSE;
    }

 /* Tabs (tabs_pri, tabs_sec) */
    $vars['tabs_pri'] = array('#theme' => 'menu_local_tasks', '#primary'   => $vars['tabs']['#primary']);
    $vars['tabs_sec'] = array('#theme' => 'menu_local_tasks', '#secondary' => $vars['tabs']['#secondary']);

 /* Logo */
    $vars['logo_title'] = $vars['site_name'] ? $vars['site_name'] : t('Home');
    if ($vars['site_slogan']) {
      $vars['logo_title'].= ': '.$vars['site_slogan'];
    }
}

/**
 * Returns HTML for primary and secondary local tasks.
 *
 * @ingroup themeable
 */

function active_n_rebuild_menu_local_tasks(&$vars) {
  $output = '';
  if (!empty($vars['primary'])) {
    $tabs_num = count($vars['primary']);
    for ($i = 0; $i < $tabs_num; $i++) {
      if (isset($vars['primary'][$i]['#link'])) {
        if ($i == 0)           $vars['primary'][$i]['#link']['localized_options']['attributes']['class'][]= 'first';
        if ($i == $tabs_num-1) $vars['primary'][$i]['#link']['localized_options']['attributes']['class'][]= 'last';
      }
    }
    $vars['primary']['#prefix'] = '<ul class="tabs_pri">';
    $vars['primary']['#suffix'] = '</ul>';
    if (theme_get_setting('nx_invisible_links')) {
      $vars['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>' . $vars['primary']['#prefix'];
    }
    $output = drupal_render($vars['primary']);
  }

  if (!empty($vars['secondary'])) {
    $tabs_num = count($vars['secondary']);
    for ($i = 0; $i < $tabs_num; $i++) {
      if (isset($vars['secondary'][$i]['#link'])) {
        if ($i == 0)           $vars['secondary'][$i]['#link']['localized_options']['attributes']['class'][]= 'first';
        if ($i == $tabs_num-1) $vars['secondary'][$i]['#link']['localized_options']['attributes']['class'][]= 'last';
      }
    }
    $vars['secondary']['#prefix'] = '<ul class="tabs_sec">';
    $vars['secondary']['#suffix'] = '</ul>';
    if (theme_get_setting('nx_invisible_links')) {
      $vars['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>' . $vars['secondary']['#prefix'];
    }
    $output = drupal_render($vars['secondary']);
  }
  return $output;
}

/**
 * Returns HTML for a single local task link.
 *
 * @param $vars
 *   An associative array containing:
 *   - element: A render element containing:
 *     - #link: A menu link array with 'title', 'href', and 'localized_options'
 *       keys.
 *     - #active: A boolean indicating whether the local task is active.
 *
 * @ingroup themeable
 */
function active_n_rebuild_menu_local_task($vars) {
  $link = &$vars['element']['#link'];
  $link['title'] = '<span class="cv-1 cv-n"><span class="cv-2 cv-n"><span class="cv-3 cv-n">'.$link['title'].'</span></span></span>';
  $link['localized_options']['html'] = TRUE;
  return theme_menu_local_task($vars);
}

/**
 * Returns HTML for a button form element.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #attributes, #button_type, #name, #value.
 *
 * @ingroup themeable
 */
function active_n_rebuild_button($vars) {
  $element = $vars['element'];
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array('id', 'name', 'value'));
  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled';
  }
  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  if (theme_get_setting('nx_btn_mode')) {
    $class = array('button-ex');
    if (isset($element['#attributes']['id'])) $class[]= 'button-ex-id-'.$element['#attributes']['id'];
    return '<span class="'.implode(' ', $class).'"><span class="cv-1 cv-n"><span class="cv-2 cv-n"><span class="cv-3 cv-n">'.$output.'</span></span></span></span>';
  } else {
    return $output;
  }
}

/**
 * Returns HTML for a fieldset form element and its children.
 *
 * @param $vars
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #attributes, #children, #collapsed, #collapsible,
 *     #description, #id, #title, #value.
 *
 * @ingroup themeable
 */

function active_n_rebuild_fieldset($vars) {
  $element = $vars['element'];
  element_set_attributes($element, array('id'));
  _form_set_class($element, array('form-wrapper'));

  $elements['attributes'] = drupal_attributes($element['#attributes']);
  $elements['title'] = isset($element['#title']) ? $element['#title'] : NULL;
  $elements['description'] = isset($element['#description']) ? $element['#description'] : NULL;
  $elements['children'] = isset($element['#children']) ? $element['#children'] : NULL;
  $elements['value'] = isset($element['#value']) ? $element['#value'] : NULL;
  return theme('nx-fieldset', array('elements' => $elements));
}

/**
 * Override or insert variables into the maintenance page template.
 */
function active_n_rebuild_preprocess_maintenance_page(&$vars) {
  // While markup for normal pages is split into page.tpl.php and html.tpl.php,
  // the markup for the maintenance page is all in the single
  // maintenance-page.tpl.php template. So, to have what's done in
  // active_n_rebuild_preprocess_html() also happen on the maintenance page, it has to be
  // called here.
  active_n_rebuild_preprocess_html($vars);
}

/**
 * Override or insert variables into the html template.
 */
function active_n_rebuild_preprocess_html(&$vars) {
    drupal_add_library('system', 'jquery.cookie');
 // Add conditional CSS for IE6-IE9.
    drupal_add_css($GLOBALS['path_to_this_theme'].'/nx-style-ie-6.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 6', '!IE' => FALSE), 'preprocess' => FALSE));
    drupal_add_css($GLOBALS['path_to_this_theme'].'/nx-style-ie-7.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
    drupal_add_css($GLOBALS['path_to_this_theme'].'/nx-style-ie-8.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 8', '!IE' => FALSE), 'preprocess' => FALSE));
}

/**
 * Override or insert variables into the html template.
 */
function active_n_rebuild_process_html(&$vars) {
}

/**
 * Override or insert variables into the page template.
 */
function active_n_rebuild_process_page(&$vars) {
}

/**
 * Override or insert variables into the region template.
 */
function active_n_rebuild_preprocess_region(&$vars) {
  $vars['classes_array'][] = 'clearfix';
}

/**
 * Override or insert variables into the node template.
 */
function active_n_rebuild_preprocess_node(&$vars) {
  $vars['submitted'] = $vars['date'] . ' — ' . $vars['name'];
}

/**
 * Override or insert variables into the comment template.
 */
function active_n_rebuild_preprocess_comment(&$vars) {
  $vars['submitted'] = $vars['created'] . ' — ' . $vars['author'];
}

/**
 * Override or insert variables into the block template.
 */
function active_n_rebuild_preprocess_block(&$vars) {
  $vars['classes_array'][] = 'clearfix';
  $vars['title_attributes_array']['class'][] = 'block_title';
}

/**
 * Returns HTML for a table.
 */
function active_n_rebuild_table($vars) {
/*
  $header = &$vars['header'];
  $rows = &$vars['rows'];
  $attributes = &$vars['attributes'];

  $attributes['class'][] = 'table';

  if (count($header)) {
    $i = 0;
    foreach ($header as &$cell) {
      if (is_array($cell)) {
        $cell['class'][]= 'tc-'.(++$i);
      } else {
        $cell = array('data'  => $cell, 'class' => 'tc-'.(++$i));
      }
    }
  }

  if (count($rows)) {
    foreach ($rows as $row_idx => $row) {
      $i = 0;
      if (isset($row['data'])) {
        foreach ($row['data'] as $cell_idx => $cell) {
          if (is_array($cell)) {
            $rows[$row_idx]['data'][$cell_idx]['class'][]= 'tc-'.(++$i);
          } elseif ($cell) {
            $rows[$row_idx]['data'][$cell_idx] = array('data'  => $cell, 'class' => array('tc-'.(++$i)));
          }
        }
      } else {
        foreach ($row as $cell_idx => $cell) {
          if (is_array($cell)) {
            $rows[$row_idx][$cell_idx]['class'][]= 'tc-'.(++$i);
          } elseif ($cell) {
            $rows[$row_idx][$cell_idx] = array('data'  => $cell, 'class' => array('tc-'.(++$i)));
          }
        }
      }
    }
  }
*/
  return theme_table($vars);
}

/**
 * Returns HTML for a menu link and submenu.
 *
 * @param $vars
 *   An associative array containing:
 *   - element: Structured array data for a menu link.
 *
 * @ingroup themeable
 */
function active_n_rebuild_menu_link(array $vars) {
    $element = $vars['element'];
 /* add depth class */
    if (isset($vars['element']['#original_link']['depth'])) {
      $depth = $vars['element']['#original_link']['depth'];
      $element['#localized_options']['attributes']['class'][]= 'depth-'.$depth;
      $element['#localized_options']['attributes']['class'][]= 'mlid-'.$element['#original_link']['mlid'];
      $element['#attributes']['class'][]= 'depth-'.$depth;
      if (in_array('collapsed', $element['#attributes']['class'])) $element['#attributes']['class'][]= 'depth-'.$depth.'-collapsed';
      if (in_array('expanded',  $element['#attributes']['class'])) $element['#attributes']['class'][]= 'depth-'.$depth.'-expanded';
      if (in_array('leaf',      $element['#attributes']['class'])) $element['#attributes']['class'][]= 'depth-'.$depth.'-leaf';
    }
 /* build link */
    $sub_menu = '';
    if ($element['#below']) {
      $sub_menu = drupal_render($element['#below']);
    }
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
    return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}




